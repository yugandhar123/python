import argparse
import json

import requests
from time import strftime


def eval_json(x):
    dicti = json.loads(x)
    assert isinstance(dicti, dict)
    return dicti


parser = argparse.ArgumentParser()
parser.add_argument("url_data", type=eval_json)
args = parser.parse_args()

app_url = args.url_data
#API_ENDPOINT = "http://172.31.27.5/gateway/productImport"
API_ENDPOINT = "http://13.126.240.62/gateway/productDelete"

headers = {'Content-type': 'application/json'}
print(str(strftime("%Y-%m-%d %H:%M:%S")))
r = requests.post(url=API_ENDPOINT, json=app_url, headers=headers)
#print(r.status_code, r.reason, r.text, r.elapsed.total_seconds())
print("req status code:",str(r.status_code) +"  "+ str(strftime("%Y-%m-%d %H:%M:%S")))
#print(r.text)
