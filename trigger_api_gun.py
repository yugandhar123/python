import multiprocessing
import subprocess

from time import strftime
import pymysql
import json


def process_urls(job_id):
    process_time = strftime("%Y-%m-%d %H:%M:%S")
    print("%s\t API Job Id: %s" % (process_time, job_id))
    cmd = 'process.py'
    job_id = "%s" % job_id
    log = open(
        '../logs/process_urls.txt',
        'a')
    log.flush()
    p = subprocess.Popen(['python3', cmd, job_id], stdout=log, stderr=log)
    p.wait()


start_date = "%s 00:00:00" % strftime("%Y-%m-%d")
end_date = "%s 23:59:59" % strftime("%Y-%m-%d")

#proc_count = multiprocessing.cpu_count()
#if proc_count > 2:
#    proc_count =  50
proc_count = 20
connection = pymysql.connect(host="13.235.245.185", user="apiUser", passwd="Enprocon@123", db="enprocon",
                             cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()


query ="""
SELECT 
   JSON_OBJECT('category',
           ic.category_id,
           'products',
           JSON_ARRAY(JSON_OBJECT('name',
                           i.item_title,
                           'seoname',
                           '',
                           'description',
                           '',
                           'make',
                           i.make,
                           'model',
                           i.model,
                           'sourceItemId',
                           i.source_item_id,
                           'vendor',
                           '15091',
                           'buyingFormat',
                           'Auction',
                           'serialNumber',
                           i.serial_number,
                           'year',
                           i.year,
                           'price',
                           i.price,
                           'day_price',
                           i.price,
                           'week_price',
                           i.price,
                           'month_price',
                           i.price,
                           'sourceurl',
                           i.item_url,
                           'notes',
                           i.notes,
                           'auctionurl',
                           '',
                           'usageKM',
                           '',
                           'usageHR',
                           '',
                           'currency',
                           'USD',
                           'city',
                          '',
                           'state',
                           '',
                           'country',
                           'USA',
                           'images',
                           JSON_ARRAY(JSON_OBJECT('path',
                                           CONCAT('https://www.rbauction.com//equipment_images/',
                                                   SUBSTRING_INDEX(i.item_url, '-', - 1),
                                                   '/large/',
                                                   i.source_item_id,
                                                   '_1.jpg'),
                                           'isPrimary',
                                           '1'))))) AS productimport
FROM
   item_bkp i 
   LEFT JOIN `item_category` AS ic ON ic.`item_key` = i.`item_key` and ic.`category_id` is not null
JOIN `category` AS c3 ON c3.`category_key` = ic.`category_key` 
LEFT JOIN `category` AS c2 ON c2.`category_key` = c3.`parent_category_key`
LEFT JOIN `category` AS c1 ON c1.`category_key` = c2.`parent_category_key`
LEFT JOIN `source` AS s ON s.`source_key` = 1  LIMIT %s OFFSET %s;

"""


query1 = """

SELECT 
  JSON_OBJECT('category',
          ic.category_id,
          'products',
          JSON_ARRAYAGG(JSON_OBJECT('name',
                          i.item_title,
                          'seoname',
                          '',
                          'description',
                          '',
                          'make',
                          ifnull(i.make,''),
                          'model',
                          ifnull(i.model,''),
                          'sourceItemId',
                          concat('T',i.item_key),
                          'vendor',
                          25,
                          'buyingFormat',
                          'auction',
                          'serialNumber',
                          ifnull(i.serial_number,''),
                          'year',
                          ifnull(i.year,''),
                          'price',
                          ifnull(i.price,''),
                          'day_price',
                          ifnull(i.price,''),
                          'week_price',
                           ifnull(i.price,''),
                          'month_price',
                          ifnull(i.price,''),
                          'sourceurl',
                          i.item_url,
                          'modelFamily',
			  ifnull(concat(mf.make , ' ',mf.model_family),''),
			   'notes',
                           ifnull(i.notes,''),
                          'auctionurl',
                          '',
                          'usageKM',
                          '',
                          'usageHR',
                          '',
                          'currency',
                          'USD',
                          'city',
                           ifnull(JSON_UNQUOTE(JSON_EXTRACT(notes, '$.city')),''),
                          'state',
                           ifnull(JSON_UNQUOTE(JSON_EXTRACT(notes, '$.state')),''),
                          'country',
                           ifnull(JSON_UNQUOTE(JSON_EXTRACT(notes, '$.country')),'USA'),
						  'images',
                          JSON_ARRAY(
                         JSON_OBJECT('path', COALESCE(i.thumbnail_s3_path,(select i_m.url FROM
   item_images AS i_m
WHERE
   i_m.item_key = i.item_key limit 1),i.thumbnail_url,""),
                                          'isPrimary',
                                          '1')
                                          )))) AS productimport
FROM
  item i
      LEFT JOIN
  `item_category` AS ic ON ic.`item_key` = i.`item_key`
	  LEFT JOIN
   `item_source` AS iss on iss.`item_key` = i.`item_key`
         JOIN
  `category` AS c3 ON c3.`category_key` = ic.`category_key`
      LEFT JOIN
  `category` AS c2 ON c2.`category_key` = c3.`parent_category_key`
      LEFT JOIN
  `category` AS c1 ON c1.`category_key` = c2.`parent_category_key`
	LEFT JOIN
 `modelFamily` AS mf on mf.`make` =i.`make` and mf.`model` = i.`model`
  WHERE iss.`source_key` != 1
  group by ic.category_id , iss.source_key , i.make , i.model 
 LIMIT %s OFFSET %s;

"""

query2 ="""
SELECT 
    JSON_OBJECT('products',JSON_ARRAYAGG(
            JSON_OBJECT('id', sourceItemId))) as itemId
FROM
    toDelete  group by make
	 LIMIT %s OFFSET %s;;
"""
# cursor.execute("SELECT count(*) FROM sanjay")
# data = cursor.fetchone()

batch_size = 10000

for offset in range(0, 100000, batch_size):
    api_format = []
    cursor.execute(query1, (batch_size, offset))
    for row in cursor.fetchall():
    	api_format.append(row.get('productimport'))

    print("Processors: %s" % proc_count)
    pool = multiprocessing.Pool(processes=proc_count)
    pool.map(process_urls, api_format, chunksize=1)  # map call in certain order
    pool.close()

